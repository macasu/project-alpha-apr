from django.shortcuts import render, redirect
from tasks.forms import TaskForm
from tasks.models import Task
from django.contrib.auth.decorators import login_required


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        # do not assign owner of task.
        if form.is_valid():
            # so use form.save() not form.save(False)
            form.save()
            # redirect to list of projects
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {"form": form}
    return render(request, "task/create_task.html", context)


@login_required
def show_assigned_task(request):
    # filter task according to the current user
    assigned_task = Task.objects.filter(assignee=request.user)
    context = {"assigned_task": assigned_task}
    return render(request, "task/task_list.html", context)
