from django.urls import path
from tasks.views import create_task, show_assigned_task

urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", show_assigned_task, name="show_my_tasks"),
]
